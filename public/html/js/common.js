$(document).ready(function($){
	// Datepicker
    $('.datapicker-control').datepicker({
        inline: true,
        showOtherMonths: true,
        dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dateFormat : 'd-m-yy',
        minDate: 0,
        showOn: 'both',
        buttonImageOnly: true,
        buttonImage: "images/calendar.png"
    });

	//https://maxfavilli.com/jquery-tag-manager
	
	$( ".tags-form").each(function() {
		var tagsParent = $(this);
		tagsParent.find(".tm-input").tagsManager({
			AjaxPush: null,
			AjaxPushAllTags: true,
			AjaxPushParameters: { 'authToken': 'foobar' }
		});
		
		tagsParent.find('.btn-addtag').on('click', function (e) {
			e.preventDefault();

			var tag = tagsParent.find(".tm-input").val();

			tagsParent.find(".tm-input").tagsManager('pushTag', tag);
		});
	});
	
	$(".nano").nanoScroller();
	function showDialog2() {
		$("#jobdetail-popup").removeClass("fade").modal("hide");
		$("#cv-popup").modal("show").addClass("fade");
	}

	$("#dialog-ok").on("click", function() {
		showDialog2();
	});
}); //end document ready