<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('curriculum_vitae_id');
            $table->string('name', 255);
            $table->string('company_name', 255)->nullable();
            $table->integer('position_id')->nullable();
            $table->string('address')->nullable();
            $table->date('start_work')->nullable();
            $table->date('end_work')->nullable();
            $table->integer('salary')->default(0);
            $table->longText('skills')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('experiences');
    }
}
