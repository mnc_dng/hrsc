<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alpha_id', 45);
            $table->string('name', 255);
            $table->string('alias', 255);
            $table->bigInteger('career_id');
            $table->bigInteger('position')->default(0);
            $table->text('description');
            $table->tinyInteger('status')->default(0);
            $table->bigInteger('author')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('careers');
    }
}
