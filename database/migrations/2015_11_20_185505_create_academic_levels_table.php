<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_levels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('curriculum_vitae_id');
            $table->string('school_name', 255);
            $table->bigInteger('diploma_id')->nullable();
            $table->string('address')->nullable();
            $table->integer('specialize_id')->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->integer('décription')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('academic_levels');
    }
}
