<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculumVitaesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculum_vitaes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('alpha_id', 45)->nullable();
            $table->bigInteger('candidate_id');
            $table->string('alias', 255)->nullable();
            $table->string('full_name', 255);
            $table->string('place_birth', 255)->nullable();
            $table->string('place_live', 255)->nullable();
            $table->string('email', 255);
            $table->date('birthday');
            $table->tinyInteger('gender')->default(0);
            $table->string('mobile', 15)->nullable();
            $table->string('phone', 15)->nullable();
            $table->text('introduct')->nullable();
            $table->text('avatar')->nullable();
            $table->string('position', 255)->nullable();
            $table->text('career_goals')->nullable();
            $table->integer('position_id')->nullable();
            $table->bigInteger('desired_salary')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('curriculum_vitaes');
    }
}
