<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('curriculum_vitae_id');
            $table->integer('skill_id')->nullable();
            $table->bigInteger('position')->default(0);
            $table->bigInteger('position_id');
            $table->tinyInteger('status')->default(0);
            $table->integer('month_expers')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('candidate_languages');
    }
}
